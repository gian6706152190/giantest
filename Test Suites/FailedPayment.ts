<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>FailedPayment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ae4c3cc8-8720-4d68-8b03-bc38670073db</testSuiteGuid>
   <testCaseLink>
      <guid>5e4d9c15-e5fe-405b-94aa-64aec3175ecf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3057720e-eb03-4df4-b6b9-da842f1b2231</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CreditPayment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8bb3ca5a-a2f9-4f93-a96a-68e5cfd2c534</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/FailedCardNumber</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cac3756-d579-44db-b3b8-27207b28a1e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/InputData</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d67649d-f92d-4b93-8975-525d6e1e41c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ButtonPayNow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42d0a934-a538-4d66-84ef-a49070194ccb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/InputOtp</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>687af577-7a0e-4770-8f98-724c2d9aa882</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VerifyFailed</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
