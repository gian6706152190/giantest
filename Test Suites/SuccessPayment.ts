<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SuccessPayment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ae6a5fcc-fe5a-4265-a04f-0286d04c1189</testSuiteGuid>
   <testCaseLink>
      <guid>9d08d4ce-0d59-4d4b-96b9-2039aa5958ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e6d3065-5cd1-445e-9d98-c149873d9d39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CreditPayment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bec782e1-058f-4ac1-a12c-82ae90b15c28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SuccessCardNumber</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b048bde2-6a54-4c96-96b4-ca232bcc3705</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/InputData</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eeea8b53-e962-4109-8b71-4b70a7398832</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ButtonPayNow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>465e40f4-3e92-4929-9dca-d0fbbde10678</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/InputOtp</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c7d8d0d-9cf1-4534-a1fa-9b198dac8b19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VerifySuccess</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
