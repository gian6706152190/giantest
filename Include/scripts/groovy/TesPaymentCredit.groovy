import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class TesPaymentCredit {

	@Given("User want buy with credit card")
	def navigationToLoginPage() {
		println ("Buy With Credit ")
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://demo.midtrans.com/')
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_BUY NOW'))
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_CHECKOUT'))
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Continue'))
		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Credit CardPay with Visa MasterCard JCB o_fd4abe'))
	}

	@When("User enters (.*) , (.*), (.*)")
	def Data(String cardnumber ,String expirydate,String cvv  ) {
		println ()
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_sample-store-1582020595_cardnumber'), cardnumber)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input'), expirydate)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_1'), cvv)
	}

	@And("User click button paynow")
	def PayNow() {
		println (" ")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Pay Now'))
	}

	@Then("User get notif Success")
	def Success() {
		println ()
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sample Store/input_Password_PaRes (1)'), '4tAN/DuJV7Y=')
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/button_OK (1)'))
		WebUI.verifyElementPresent(findTestObject('Page_Sample Store/div_Thank you for your purchaseGet a nice sleep'), 0)
		WebUI.delay(2)
		WebUI.closeBrowser()
	}

	@Then("User get notif Failed")
	def Failed() {
		println ()
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sample Store/input_Password_PaRes (1)'), '4tAN/DuJV7Y=')
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/button_OK (1)'))
		WebUI.verifyElementPresent(findTestObject('Page_Sample Store/span_Transaction failed'), 0)
		WebUI.verifyElementPresent(findTestObject('Page_Sample Store/span_Your card got declined by the bank'), 0)
		WebUI.delay(2)
		WebUI.closeBrowser()
	}
}