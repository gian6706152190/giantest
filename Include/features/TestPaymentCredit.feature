Feature: Buy with Credit Card Feature

  @Success
  Scenario Outline: Success Credit payment
    Given User want buy with credit card
    When User enters <cardnumber> , <expirydate> , <cvv>
    And User click button paynow
    Then User get notif Success

    Examples: 
      | cardnumber          | expirydate | cvv | bankotp | status  |
      | 4811 1111 1111 1114 | 02/20      | 123 |  112233 | success |

  @Failed
  Scenario Outline: Failed Credit payment
    Given User want buy with credit card
    When User enters <cardnumber> , <expirydate> , <cvv>
    And User click button paynow
    Then User get notif Failed

    Examples: 
      | cardnumber          | expirydate | cvv | bankotp | status |
      | 4911 1111 1111 1113 | 02/20      | 123 |  112233 | failed |
